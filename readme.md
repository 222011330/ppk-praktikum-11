Nama : Arnoldy Fatwa Rahmadin

NIM : 222011330

Kelas : 3SI3

Praktikum Pemrograman Platform Khusus Pertemuan 11

- Dokumentasi Praktikum
![btn1](dokumentasi/btn1.jpg)
![btn2](dokumentasi/btn2.jpg)


1. Setting Button 3 (Melakukan action web search dengan kata kunci "Intent Android")

    - Tampilan setelah di klik Button 3
    ![no1](dokumentasi/no1.jpg)

2. Setting Button 3 (Melakukan direct ke file manager untuk mendapatkan file bertipe audio/mp3)

    - Tampilan setelah di klik Button 3
    ![no2](dokumentasi/no2.jpg)

3. Membuat aplikasi pengisian data diri yang terdiri dari 2 activity

    - Bagian Awal Form (Halaman Activity 1)
    ![input](dokumentasi/input.jpg)

    - Dilakukan Pengisian Form, kemudian meng klik tombol simpan dan kirim
    ![input2](dokumentasi/input2.jpg)

    - Tampilan Output (Redirect ke Halaman Activity 2)
    ![output](dokumentasi/output.jpg)

